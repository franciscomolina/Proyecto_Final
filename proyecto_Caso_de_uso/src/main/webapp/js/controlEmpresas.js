var appEmpresas = angular.module('appEmpresas',[]);

appEmpresas.controller('controlEmpresas', ['$scope', '$http', "$log", 
    function($scope, $http, $log){
        $log.debug('Definiendo controlador...');
        
        $scope.IsHidden = true;
        
        // Obtener la lista de alumnos
        $scope.lasEmpresas = [];
        
        $scope.cargarEmpresas= function(){
            
            $http.get('empresa')
                 .then( function( respuesta ){
                   $scope.lasEmpresas =respuesta.data;
                   $scope.IsHidden = false;

                 });
        };
        
        
        $scope.cargarEmpresasByBusqueda= function(nombre){
            
            $http.get('empresa' + '/' + nombre )
                 .then( function( respuesta ){
                   $scope.lasEmpresas =respuesta.data;
                   $scope.IsHidden = false;

                 });
        };
        
        $scope.cargarEmpresasByBusquedaRupc= function(rupc){
            
            $http.get('empresa' + '/byrupc/' + rupc )
                 .then( function( respuesta ){
                   $scope.lasEmpresas =respuesta.data;
                   $scope.IsHidden = false;

                 });
        };
        
        $scope.cargarEmpresasByBusquedaRfc= function(rfc){
            
            $http.get('empresa' + '/byrfc/' + rfc )
                 .then( function( respuesta ){
                   $scope.lasEmpresas =respuesta.data;
                   $scope.IsHidden = false;

                 });
        };
}]);

