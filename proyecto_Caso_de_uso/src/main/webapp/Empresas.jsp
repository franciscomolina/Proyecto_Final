<%-- 
    Document   : Empresas
    Created on : 20/08/2018, 09:39:11 PM
    Author     : f_mol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appEmpresas">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="js/angular.min.js"></script>
        <script src="js/controlEmpresas.js"></script>
        <title>EMPRESAS</title>
        <style type="text/css">
            html, body {
                background-color: #ecf0f1;
                margin: 20px auto;
                display: block;
                max-width: 600px;
                height: 100%;
                text-align: center;
              }

              body {
                padding: 20px;
                position: relative;
              }
              h2, a, p, *:before,h1 {
                font-family: "Helvetica Neue", sans-serif;
                font-weight: 300;
              }
              h1 {
                color: #3498db;
              }

              h2 {
                color: #2980b9;
                margin-top: 0;
                font-size: 20px;
                background-color: white;
                height: 24px;
                text-align: center;
                padding: 16px;
                z-index: 15;
                border-radius: 4px;

                transition: all 2s ease-out;
              }

              p {
               display: block; 
                width: 100%;
                color: #2c3e50;
                clear: both;
              }
              .description {
                margin-bottom: 70px;
              }

              button {
                border: 0;
                background-color: #3498db;
                color: white;
                border-radius: 4px;
                padding: 5px 10px;
                transition: background-color 0.2s ease-out;


                cursor: pointer;
              }

              button:hover {
                background-color: #2980b9;
              }
              button:active, button:hover {
                outline: none;
              }

              a {
                color: #2c3e50;
                transition: color 0.2s ease-out;
                /*padding: 5px 10px;*/
                margin-right: 16px;
              }

              a:hover {
                color: #2ecc71;
              }

              .wrapper {
                border: 1px dashed #95a5a6;
                height: 56px;
                margin-top: 16px;
                border-radius: 4px;
                position: relative;
                font-size: 12px;
              }

              .wrapper p {
                line-height: 31px;
              }
              </style>
    </head>
    <body>
        <h1>Consulta de Empresas Registradas</h1>
        <div ng-controller="controlEmpresas">
            
            <h3>Nombre Empresa</h3>
            <input ng-model="laBusqueda" type="search" value="Nombre Empresa"  ng-click="" ng-click="showme">
            <input type="button" value="Buscar por NOMBRE" ng-click="cargarEmpresasByBusqueda(laBusqueda)" ng-click="showme">
            <input type="button" value="Buscar por RUPC" ng-click="cargarEmpresasByBusquedaRupc(laBusqueda)" ng-click="showme">
            <input type="button" value="Buscar por RFC" ng-click="cargarEmpresasByBusquedaRfc(laBusqueda)" ng-click="showme">
            
            <div ng-hide = "IsHidden">
              <div>
                <h4>Empresas</h4>
                <ul ng-repeat="e in lasEmpresas">
                    <li ng-model="" ng-value="e.id">{{ e.nombre }} , {{ e.rupc }}, {{ e.rfc }} 
                       
                    </li>
                </ul>
            </div>
            </div>
                            
                            
            
</html>
