/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.proyecto_caso_de_uso.control;

import java.util.List;
import mrysi.proyecto_caso_de_uso.entidades.Empresas;
import mrysi.proyecto_caso_de_uso.entidades.Entidades;
import mrysi.proyecto_caso_de_uso.oad.OadEntidades;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author f_mol
 */

@RestController
@RequestMapping("/entidades")
public class ControladorEntidades {
    
    private final static Logger LOG = LoggerFactory.getLogger(ControladorEntidades.class);
    
    @Autowired
    OadEntidades oadEntidades;
    
    @GetMapping("")
    public List<Entidades> getTodas() {
        LOG.debug("GET");
        return oadEntidades.findAll();
    }
    
    // Obtener empresas de una eentidad utilizando su id
    @GetMapping("getempresas/{id}")
    public List<Empresas> getEmpresasByIdEntidades(@PathVariable("id") Integer id) {
        LOG.debug("GET");

        return (List)oadEntidades.findOne(id).getEmpresasCollection();

    }
}
