/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.proyecto_caso_de_uso.control;

import java.util.List;
import mrysi.proyecto_caso_de_uso.entidades.Empresas;
import mrysi.proyecto_caso_de_uso.oad.OadEmpresas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author f_mol
 */
@RestController
@RequestMapping("/empresa")

public class ControladorEmpresas {
    private final static Logger LOG = LoggerFactory.getLogger(ControladorEmpresas.class);
    
    @Autowired
    OadEmpresas oadEmpresas;
    
    @GetMapping(value="" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Empresas> getAllEmpresas() {
        LOG.debug("GET");
        return oadEmpresas.findAll();
    }

    @GetMapping(value="/{nombre}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Empresas> getEmpresaByNombre(@PathVariable("nombre") String nombre) {
        LOG.debug("GET");
        return oadEmpresas.findByNombre(nombre);
    }
    
    @GetMapping(value="/byrupc/{rupc}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Empresas> getEmpresaByRupc(@PathVariable("rupc") String rupc) {
        LOG.debug("GET");
        return oadEmpresas.findByRupc(rupc);
    }
    
    @GetMapping(value="/byrfc/{rfc}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Empresas> getEmpresaByRfc(@PathVariable("rfc") String rfc) {
        LOG.debug("GET");
        return oadEmpresas.findByRfc(rfc);
    }

    
    

    
}
