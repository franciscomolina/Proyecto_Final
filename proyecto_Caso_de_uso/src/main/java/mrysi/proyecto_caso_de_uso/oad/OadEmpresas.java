/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.proyecto_caso_de_uso.oad;

import java.util.List;
import mrysi.proyecto_caso_de_uso.entidades.Empresas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
/**
 *
 * @author f_mol
 */
public interface OadEmpresas extends JpaRepository<Empresas, Integer> {
    
    List<Empresas> findByNombre(@Param("nombre") String nombre);
    List<Empresas> findByRupc(@Param("rupc") String rupc);
    List<Empresas> findByRfc(@Param("rfc") String rfc);
    
}
