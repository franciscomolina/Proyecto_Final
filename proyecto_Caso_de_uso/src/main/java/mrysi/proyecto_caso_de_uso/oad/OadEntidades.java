/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.proyecto_caso_de_uso.oad;

import mrysi.proyecto_caso_de_uso.entidades.Entidades;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author f_mol
 */
public interface OadEntidades extends JpaRepository<Entidades, Integer> {
    
}
