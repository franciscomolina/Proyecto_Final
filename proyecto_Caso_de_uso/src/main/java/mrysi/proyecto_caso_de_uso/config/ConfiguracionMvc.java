package mrysi.proyecto_caso_de_uso.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 *
 * @author jaguilar
 */
@Configuration
@PropertySource("classpath:/spring.properties")
@ComponentScan(basePackages = "${spring.controladores.paquete}")
public class ConfiguracionMvc extends WebMvcConfigurationSupport {

    
    // Paso de recursos estaticos
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**/*.js").addResourceLocations("/");
        registry.addResourceHandler("/**/*.css").addResourceLocations("/");
        registry.addResourceHandler("/**/*.html").addResourceLocations("/");
        registry.addResourceHandler("/*.html").addResourceLocations("/");
    }
    
    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
        internalResourceViewResolver.setPrefix("/WEB-INF/jsp/");
        internalResourceViewResolver.setSuffix(".jsp");
        return internalResourceViewResolver;
    }

}
